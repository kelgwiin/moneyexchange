package io.moneyexchange.exchange.Constants;


import io.moneyexchange.exchange.model.Exchanger;
import io.moneyexchange.exchange.model.Rates;

public class ConstantsTest {
    public final static float EUR_RATE = 3.8585f;
    public final static String BASE = "USD";
    public final static Exchanger EXCHANGE_USD = new Exchanger("id_123456", "USD", "2018-01-01", new Rates(EUR_RATE));
}
