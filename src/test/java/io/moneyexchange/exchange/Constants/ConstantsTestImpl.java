package io.moneyexchange.exchange.Constants;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ConstantsTestImpl {
    @Test
    public void testConstansts(){
        Constants constants = new Constants();

        assert (constants.EUR.equals("EUR"));
    }

}
