package io.moneyexchange.exchange.bussiness;

import io.moneyexchange.exchange.Constants.ConstantsTest;
import io.moneyexchange.exchange.bussiness.impl.ExchangeServiceImpl;
import io.moneyexchange.exchange.model.Exchanger;
import io.moneyexchange.exchange.model.InputQueryParam;
import io.moneyexchange.exchange.model.Rates;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ExchangeServiceTestImpl {

    @TestConfiguration
    static class ExchangeImplTestContextConfiguration {

        @Bean
        public ExchangeService employeeService() {
            return new ExchangeServiceImpl();
        }
    }

    @Autowired
    ExchangeService exchangeService;

    @MockBean
    private ExchangerRepository exchangerRepository;

    @Before
    public void setUp(){
        Mockito.when(exchangerRepository.findByBase(ConstantsTest.BASE)).thenReturn(ConstantsTest.EXCHANGE_USD);
    }

    @Test
    public void getExchangeRate(){
        InputQueryParam inputQueryParam = new InputQueryParam("USD", "EUR");

        assert (exchangeService.getExchange(inputQueryParam) == ConstantsTest.EUR_RATE);

    }

}
