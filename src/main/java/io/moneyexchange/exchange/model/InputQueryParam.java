package io.moneyexchange.exchange.model;

import lombok.*;


@Getter
@Setter
@ToString(doNotUseGetters = true)
@NoArgsConstructor
@AllArgsConstructor
public class InputQueryParam {
    private String base;
    private String symbols;
}
