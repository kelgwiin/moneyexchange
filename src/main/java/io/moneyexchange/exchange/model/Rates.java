package io.moneyexchange.exchange.model;

import lombok.*;


@Getter
@Setter
@ToString(doNotUseGetters = true)
@AllArgsConstructor
@NoArgsConstructor
public class Rates {
    private float EUR;
}
