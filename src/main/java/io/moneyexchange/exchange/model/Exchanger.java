package io.moneyexchange.exchange.model;

import lombok.*;
import org.springframework.data.annotation.Id;

/**
 * <br/>
 * <b>Class</b>: Exchanger<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 * <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li> Kelwin Gamez Barradas (KGB) From (EVE)</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>5/8/18 (KGB) Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@Getter
@Setter
@ToString(doNotUseGetters = true)
@AllArgsConstructor
@NoArgsConstructor
public class Exchanger {
    @Id
    private String id;

    private String base;
    private String date;
    private Rates rates;
}
