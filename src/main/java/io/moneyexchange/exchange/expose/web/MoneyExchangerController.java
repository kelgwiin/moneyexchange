package io.moneyexchange.exchange.expose.web;

import io.moneyexchange.exchange.bussiness.ExchangeService;
import io.moneyexchange.exchange.model.InputQueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <br/>
 * <b>Class</b>: MoneyExchangerController<br/>
 *
 * @author Kelwin Gamez <br />
 * <u>Developed by</u>: <br/>
 * <ul>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>5/7/18 (KGB) Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
@RestController
@RequestMapping("/exchanger")
public class MoneyExchangerController {
    @Autowired
    ExchangeService exchangeService;
    @CrossOrigin(origins = "*")
    @GetMapping(value = "/latest", produces = MediaType.APPLICATION_JSON_VALUE)
    public float getExchange(InputQueryParam inputQueryParam) {
        return exchangeService.getExchange(inputQueryParam);
    }
}
