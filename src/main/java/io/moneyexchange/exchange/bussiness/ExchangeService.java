package io.moneyexchange.exchange.bussiness;

import io.moneyexchange.exchange.model.InputQueryParam;

/**
 * <br/>
 * <b>Class</b>: ExchangeService<br/>
 * <b>Copyright</b>: &copy; 2018 Banco de Cr&eacute;dito del Per&uacute;<br/>
 * <b>Company</b>: Banco de Cr&eacute;dito del Per&uacute;<br/>
 *
 * @author Banco de Cr&eacute;dito del Per&uacute; (BCP) <br/>
 * <u>Service Provider</u>: Everis Per&uacute; SAC (EVE) <br/>
 * <u>Developed by</u>: <br/>
 * <ul>
 * <li> Kelwin Gamez Barradas (KGB) From (EVE)</li>
 * </ul>
 * <u>Changes</u>:<br/>
 * <ul>
 * <li>5/7/18 (KGB) Creaci&oacute;n de Clase.</li>
 * </ul>
 * @version 1.0
 */
public interface ExchangeService {
    float getExchange(InputQueryParam inputQueryParam);
}
