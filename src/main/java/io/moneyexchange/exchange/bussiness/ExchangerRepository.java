package io.moneyexchange.exchange.bussiness;

import io.moneyexchange.exchange.model.Exchanger;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface ExchangerRepository extends MongoRepository<Exchanger, String>{
    Exchanger findByBase(String base);
}
