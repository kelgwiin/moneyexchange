package io.moneyexchange.exchange.bussiness.impl;

import io.moneyexchange.exchange.Constants.Constants;
import io.moneyexchange.exchange.bussiness.ExchangerRepository;
import io.moneyexchange.exchange.bussiness.ExchangeService;
import io.moneyexchange.exchange.model.Exchanger;
import io.moneyexchange.exchange.model.InputQueryParam;
import io.moneyexchange.exchange.model.Rates;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author Kelwin Gamez
 */
@Service
@Slf4j
public class ExchangeServiceImpl implements ExchangeService {
    @Autowired
    private ExchangerRepository exchangerRepository;

    @Override
    public float getExchange(InputQueryParam inputQueryParam) {
        log.info(exchangerRepository.findByBase(inputQueryParam.getBase()).toString());

        log.info(inputQueryParam.toString());
        return getRateBySymbol(inputQueryParam);
    }

    private float getRateBySymbol(InputQueryParam inputQueryParam) {
        Optional<Rates> ratesOptional = Optional.ofNullable(exchangerRepository.findByBase(inputQueryParam.getBase()).getRates());
        float rate=0;

        if (ratesOptional.isPresent()) {
            switch (inputQueryParam.getSymbols()) {
                case Constants.EUR:
                    rate = ratesOptional.get().getEUR();
                    break;

                    //if you have more than one symbols add here...

                default:
                    rate = -1;
            }
        }
        return rate;
    }
}
